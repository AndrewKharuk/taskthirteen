﻿using System;
using System.IO;

namespace TaskThirteen
{
    public class FilesWorker
    {
        public event EventHandler<FileArgs> FileFound;
        public void EnumerateFilesWithEvent(string directoryPuth, string searchPattern)
        {
            if (!Directory.Exists(directoryPuth))
            {
                throw new DirectoryNotFoundException();
            }

            foreach (var file in Directory.EnumerateFiles(directoryPuth, searchPattern, SearchOption.AllDirectories))
            {
                FileFound?.Invoke(this, new FileArgs(file));
            }
        }
    }
}
