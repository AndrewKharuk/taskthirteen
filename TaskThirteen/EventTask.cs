﻿using System;

namespace TaskThirteen
{
    public class EventTask
    {
        FilesWorker woker;
        readonly string _fileNameForEnd;

        public EventTask(string fileNameForEnd = null)
        {
            _fileNameForEnd = fileNameForEnd;
        }
        public void ToDo(string directoryPuth, string searchPattern)
        {
            woker = new FilesWorker();
            woker.FileFound += Woker_Handler;
            woker.EnumerateFilesWithEvent(directoryPuth, searchPattern);
        }

        private void Woker_Handler(object sender, EventArgs e)
        {
            Console.WriteLine((e as FileArgs).FoundFile);

            if (_fileNameForEnd != null && (e as FileArgs).FoundFile.Contains(_fileNameForEnd))
            {
                woker.FileFound -= Woker_Handler;
            }
        }
    }
}
