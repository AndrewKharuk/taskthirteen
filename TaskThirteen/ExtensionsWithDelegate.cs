﻿using System;
using System.Collections.Generic;

namespace TaskThirteen
{
    public static class ExtensionsWithDelegate
    {
        /// <summary>
        /// Расширяющий метод для получения максимального класса из коллекции классов
        /// </summary>
        /// <typeparam name="T">Сравниваемый тип</typeparam>
        /// <param name="e">Коллекция классов для выбора максимального значения</param>
        /// <param name="getParametr">Делегат представляющий метод для преобразования экземпляра класса в числовое значение</param>
        /// <returns>мМаксимальный экземпляр класса</returns>
        public static T GetMaxType<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            T maxType = null;

            float maxFloat = float.MinValue;
            foreach (T currentType in e)
            {
                float currentFloat = getParametr(currentType);

                if (currentFloat > maxFloat)
                {
                    maxFloat = currentFloat;
                    maxType = currentType;
                }
            }

            return maxType;
        }
    }

    //Классы для тестирования
    public class TestClassEmpty
    {
        public override string ToString()
        {
            return GetHashCode().ToString();
        }
    }

    public class TestClassCustomer
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public TestClassCustomer(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public override string ToString()
        {
            return $"{Name} - {Age}";
        }
    }
}
