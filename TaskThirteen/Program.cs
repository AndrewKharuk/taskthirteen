﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskThirteen
{
    class Program
    {
        static void Main(string[] args)
        {
            // For delegate
            TestClassEmpty[] testClasses = { new TestClassEmpty(), new TestClassEmpty(), new TestClassEmpty() };
            //Console.WriteLine(string.Join("\r\n", testClasses.Select(x => x.GetHashCode())));
            TestClassEmpty result = testClasses.GetMaxType<TestClassEmpty>(x => x.GetHashCode());
            Console.WriteLine(result.ToString());

            TestClassCustomer[] customers = {new TestClassCustomer("Greg", 25),
                                             new TestClassCustomer("Mike", 55),
                                             new TestClassCustomer("Nill", 15)};
            TestClassCustomer result2 = customers.GetMaxType<TestClassCustomer>(x => x.Age);
            Console.WriteLine(result2.ToString());


            ////For event
            string directoryPuth = @"D:\TestDirectory";
            string searchPattern = "*.txt";
            string fileNameForEnd = "Text2.txt";

            //EventTask eventTask = new EventTask();              // выводит все файлы
            EventTask eventTask = new EventTask(fileNameForEnd);    // выводит файлы до fileName включительно

            try
            {
                eventTask.ToDo(directoryPuth, searchPattern);
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                Console.WriteLine($"No Directory {directoryPuth}.\r\nEnter another path.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
